import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/@services/user/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  registroForm = new FormGroup({
    'name': new FormControl("", { nonNullable: true, validators: Validators.required }),
    'phone': new FormControl("", { nonNullable: true, validators: Validators.required }),
    'mail': new FormControl("", { nonNullable: true, validators: [Validators.required, Validators.email] }),
    'password': new FormControl("", { nonNullable: true, validators: [Validators.required, Validators.minLength(8)] }),
  });

  constructor(
    private userService: UserService,
    private router: Router,
  ) {

  }

  registerUser() {
    const user = this.registroForm.getRawValue();
    this.userService.addUser(user)
    this.userService.setActiveUser(user)
    this.router.navigate(['/'])
  }
}
