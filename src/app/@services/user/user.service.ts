import { Injectable } from '@angular/core';

export interface User {
  name: string;
  phone: string,
  mail: string,
  password: string
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  users: User[] = [
    {
      name: 'juan',
      phone: '12345',
      mail: 'juan@mail.com',
      password: '12345678'
    }
  ];
  activeUser?: User;

  constructor() { }

  addUser(user:User) {
    this.users.push(user);
  }

  validateCredentials(mail:string, password:string) : boolean {
    const user = this.users.find(user => user.mail == mail)
    if(!user) {
      return false;
    }
    return user.password == password;
  }

  setActiveUser(user:User) {
    this.activeUser = user;
  }

  clearActiveUser() {
    this.activeUser = undefined;
  }

  getUserByMail(mail:string): User|undefined {
    return this.users.find(user => user.mail == mail)
  }

}
